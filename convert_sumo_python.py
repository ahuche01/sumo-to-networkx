#!/usr/bin/python

"""Converts sumo data to plain python data"""

import pickle
import multiprocessing
import collections
# from typing import Dict, Tuple
from math import radians, cos, sin, asin, sqrt
import sumolib

NET_FILE = ('/home/antoine/Documents/Nextcloud/Donnees/TAPAS/'
            'TAPASCologne-0.24.0/cologne2.net.xml')
ROUTES_FILE = '/home/antoine/Documents/Nextcloud/Donnees/TAPAS/cologne.rou.xml'
# Not sure about the net file
# Can't convert XY coordinated to Lat Lon coordinates because projParameter is
# "!", which means that no projection was applied
# https://sumo.dlr.de/docs/Networks/SUMO_Road_Networks.html
# For networks with no projection (projParameter='!'), only the offset must be
# applied
# netOffset="346.83,-207.06" convBoundary="0.00,0.00,2164.41,1916.16"
# origBoundary="-346.83,0.00,1817.58,2148.55"
# BOLOGNE_NET = ('/home/antoine/Téléchargements/Bologna_small-0.29.0/joined/'
#                'joined_buslanes.net.xml')
# BOLOGNE_ROUTES = ('/home/antoine/Téléchargements/Bologna_small-0.29.0/'
#                   'joined/joined.rou.xml')
BOLOGNE_NET = ('/home/antoine/Téléchargements/joined_buslanes.net.xml')
BOLOGNE_ROUTES = ('/home/antoine/Téléchargements/joined.rou.xml')


def represents_int(string):
    """Checks if the given string can be casted into an integer"""
    try:
        int(string)
        return True
    except ValueError:
        return False


def load_network(network_file):
    """Loads the network"""
    return sumolib.net.readNet(network_file)


def parse_nodes(net):
    """Parses the nodes of the given network"""
    return [node.getID() for node in net.getNodes()]


def parse_edges(net):
    """Create list of edges"""
    return [(edge.getFromNode().getID(), edge.getToNode().getID())
            for edge in net.getEdges()]


def haversine_distance(lat1, lon1, lat2, lon2):
    """ Consistent with both another online calculator and openstreetmap + my
    good ruler """

    # Coordinate of the Jan von Werth Fountain, which I will assume is the
    # centre of Cologne: 50.9385561, 6.9600561.
    latitude_fountain = 50.9385561
    # For Bologne:
    # A small-scale scenario based on the city of Bologna. The large roundabout
    # can be found at 44.494554,11.314393.
    # latitude_fountain = 44.494554
    radius_equator = 6378.1370
    radius_pole = 6356.7523
    radius_cologne = sqrt(((radius_equator**2 * cos(latitude_fountain))**2 +
                           (radius_pole**2 * sin(latitude_fountain))**2) /
                          ((radius_equator * cos(latitude_fountain))**2 +
                           (radius_pole * sin(latitude_fountain))**2))

    lat_difference = radians(lat2 - lat1)
    lon_difference = radians(lon2 - lon1)
    lat1 = radians(lat1)
    lat2 = radians(lat2)

    return radius_cologne * 2 * asin(sqrt(sin(lat_difference/2)**2 +
                                          cos(lat1) * cos(lat2) *
                                          sin(lon_difference/2)**2))


def parse_routes_old(net, route_file):
    """Parse all edges of the route file"""
    routes = list()
    count = 0
    for route in sumolib.output.parse(route_file, 'route'):
        count += 1
        if count % 100000 == 0:
            print(count)
        rout = list()
        for edge in route.edges.split():
            rout.append((net.getEdge(edge).getFromNode().getID(),
                         net.getEdge(edge).getToNode().getID()))
        routes.append(rout)
        # routes.append([(net.getEdge(edge).getFromNode().getID(),
        #                 net.getEdge(edge).getToNode().getID())
        #                for edge in route.edges.split()])
    return routes


def multithread_timestamped_traj(route_file, start, end, pad):
    """ Multithreaded implementation of the compute_timestamped_traj. Exports
    the result to a file. Does it with a padding, because Ubuntu would kill my
    process. Sometimes this outputs "RecursionError: maximum recursion depth
    exceeded while calling a Python object". I don't know why but it seems to
    happen when called after some work has already been done. So to call this
    one (or build_timestamped_routes) first.

    ==================================================
    || This needs the NET variable to be set first. ||
    ==================================================

    """
    for i in range(start, end, pad):
        manager = multiprocessing.Manager()
        routes = manager.list()
        pool = multiprocessing.Pool(multiprocessing.cpu_count())
        pool.starmap(compute_timestamped_traj,
                     [(0, vehicle.depart,
                       vehicle.getChildList()[0].edges.split(), index, routes)
                      for (index, vehicle) in
                      enumerate(sumolib.output.parse(route_file, "vehicle"))]
                     # [:100000])
                     [i:i+pad])
        pool.terminate()
        routes = list(routes)
        print(len(routes))
        filee = open('traj_auto_{}'.format(i+pad), 'wb')
        pickle.dump(routes, filee)
        filee.close()
        print("Exported")
    # return routes


def get_length(route_file):
    """ Computes the number of routes in the file. I don't know how to do it
    better with a generator """
    length = 0
    for _ in sumolib.output.parse(route_file, "vehicle"):
        length += 1
    return length


def compute_timestamped_traj(net, depart, edges, index, routes):
    """ Computes timestamped trajectories based on the time of departure and
    the maximum speed on each edge. This assumes the driver drives at constant
    maximum speed. Based on the Haversine distance, centered at downtown
    Cologne.

    Takes routes as a parameter instead of returning it because it is sometimes
    called with multiprocessing. This is why there is an index paramenter too,
    so I can track progress. """
    print(index)
    # net == 0 when we are working with multiprocessing. We can't put the
    # network file in the arguments as it causes too many recursions. I chose
    # to store it as a global variable
    if net == 0:
        net = NET
    departure_time = float(depart)
    route = []
    for edge in edges:
        speed = net.getEdge(edge).getSpeed()
        if route:
            last_time = route[-1][1][1]
        else:
            last_time = departure_time
        distance = haversine_distance(
            *net.convertXY2LonLat(
                *net.getEdge(edge).getFromNode().getCoord()),
            *net.convertXY2LonLat(
                *net.getEdge(edge).getToNode().getCoord())) * 1000
        route.append(((net.getEdge(edge).getFromNode().getID(), last_time),
                      (net.getEdge(edge).getToNode().getID(),
                       last_time + (distance/speed))))
    routes.append(route)


def parse_routes(net, route_file):
    """Parse all edges of the route file into a list of routes, which is a list
    of edges"""
    return [[(net.getEdge(edge).getFromNode().getID(),
              net.getEdge(edge).getToNode().getID())
             for edge in route.edges.split()]
            for route in
            sumolib.output.parse(route_file, 'route')]


def map_edges_int(edge_list, mapping):
    """Map non integer nodes to integer nodes"""
    # mapping = dict()  # type: Dict[Tuple[int, int], int]
    # In this dataset, the minimum node id is 1001081517 so we are fine
    for (i, (vertex_u, vertex_v)) in enumerate(edge_list):
        if represents_int(vertex_u):
            edge_list[i] = (int(vertex_u), edge_list[i][1])
        else:
            # Needed in case a node has degree > 1
            if vertex_u not in mapping:
                mapping[vertex_u] = len(mapping) + 1
            edge_list[i] = (mapping[vertex_u], edge_list[i][1])

        if represents_int(vertex_v):
            edge_list[i] = (edge_list[i][0], int(vertex_v))
        else:
            # Needed in case a node has degree > 1
            if vertex_v not in mapping:
                mapping[vertex_v] = len(mapping) + 1
            edge_list[i] = (edge_list[i][0], mapping[vertex_v])
    return edge_list, mapping


def map_routes_int(routes, mapping):
    """Apply the same mapping to the routes"""
    for (i, route) in enumerate(routes):
        for (j, (node_u, node_v)) in enumerate(route):
            if not represents_int(node_u):
                routes[i][j] = (mapping[node_u], routes[i][j][1])
            else:
                routes[i][j] = (int(node_u), routes[i][j][1])

            if not represents_int(node_v):
                routes[i][j] = (routes[i][j][0], mapping[node_v])
            else:
                routes[i][j] = (routes[i][j][0], int(node_v))
    return routes


def map_timestamped_routes_int(routes, mapping):
    """Apply the same mapping to the routes"""
    for (i, route) in enumerate(routes):
        for (j, ((node_u, time1), (node_v, time2))) in enumerate(route):
            if not represents_int(node_u):
                routes[i][j] = ((mapping[node_u], int(time1)),
                                (routes[i][j][1][0], int(time2)))
            else:
                routes[i][j] = ((int(node_u), int(time1)),
                                (routes[i][j][1][0], int(time2)))

            if not represents_int(node_v):
                routes[i][j] = ((routes[i][j][0][0], int(time1)),
                                (mapping[node_v], int(time2)))
            else:
                routes[i][j] = ((routes[i][j][0][0], int(time1)),
                                (int(node_v), int(time2)))


def check_node_int(name_list, node):
    """Checks that a given node is an integer"""
    if not isinstance(node, int):
        print(name_list, node, "isn't int, it is", type(node))


def check_edge_list_int(edge_list):
    """Verify there is only integer nodes after the mapping"""
    for (vertex_x, vertex_y) in edge_list:
        check_node_int("Edge_list", vertex_x)
        check_node_int("Edge_list", vertex_y)
        if not isinstance(vertex_x, int):
            print(vertex_x)
        if not isinstance(vertex_y, int):
            print(vertex_y)


def check_routes_int(routes):
    """Verify all nodes of the routes are integers"""
    for route in routes:
        for (node_u, node_v) in route:
            check_node_int("Routes", node_u)
            check_node_int("Routes", node_v)


def consistency_timestamped(routes, edge_list):
    """Checks consistency for timestamped routes"""
    cpt = 0
    for route in routes:
        for edge in route:
            if (edge[0][0], edge[1][0]) not in edge_list:
                print(edge, "not in EDGE_LIST")
        cpt += 1
        print(cpt, "out of", len(routes))


def consistency(routes, edge_list):
    """Checks consistency. Just convert the edge_list to a set of couple to
    make it linear instead of quadratic. """
    cpt = 0
    for route in routes:
        for edge in route:
            if edge not in edge_list:
                print(edge, "not in EDGE_LIST")
        cpt += 1
        print(cpt, "out of", len(routes))


def dump_net_to_file(path, edge_list):
    """Dumps the road network to a file"""
    graph = open(path, 'w')
    for edge in edge_list:
        graph.write(str(edge[0]) + ' ' + str(edge[1]) + '\n')
    graph.close()


def dump_routes_to_file(path, routes):
    """Dumps the routes to a file"""
    routes_file = open(path, 'wb')
    pickle.dump(routes, routes_file)
    routes_file.close()


def nb_routes_nodes(edge_list, routes):
    """ Computes the number of routes that cross each node. """
    nb_route = dict()
    for node1, node2 in edge_list:
        nb_route[node1] = 0
        nb_route[node2] = 0
    for route in routes:
        for edge in route:
            nb_route[edge[0]] += 1
            nb_route[edge[1]] += 1
    return nb_route


def build_coordinates(net_file, mapping):
    """ Builds a dictionary with the coordinates of all nodes """
    net = load_network(net_file)
    # May load mapping_reversed for Cologne
    # mapping = get_mapping_reversed()
    coords = dict()
    total = 0
    for node in net.getNodes():
        total += 1
        # print(total, node.getID(), node.getID() in mapping)
        if node.getID() in mapping:
            # Sadly, my implementation expects (lat, lon)
            lon, lat = net.convertXY2LonLat(*node.getCoord())
            coords[mapping[node.getID()]] = lat, lon
        else:
            lon, lat = net.convertXY2LonLat(*node.getCoord())
            coords[int(node.getID())] = lat, lon
    return coords


def check_routes(routes_old, routes):
    """ check routes are consistent """
    for index1, route in enumerate(routes):
        for index2, edge in enumerate(route):
            for index3, node in enumerate(edge):
                if node[0] != routes_old[index1][index2][index3]:
                    print(node[0], routes_old[index1][index2][index3])


def get_mapping():
    """ Loads the mapping file """
    # New_ID -> Old_ID
    mapping_file = open('mapping', 'rb')
    mapping = pickle.load(mapping_file)
    mapping_file.close()
    return mapping


def get_mapping_reversed():
    """ Loads a the mapping file then reverses it.
    This is the same data as build_graph(NET_FILE)[1] """
    mapping = get_mapping()
    # Old_ID -> New-ID
    mapping_reversed = dict()
    for key, value in mapping.items():
        mapping_reversed[value] = key
    return mapping_reversed


def map_routes_int_timestamp(start, end, pad, mapping):
    """ Maps the routes to integer values following MAPPING (through
    mapping_reversed). Also maps the timestamps to integers values. We don't
    need floating precision."""
    for i in range(start, end, pad):
        print(i+pad)
        routes_file = open('traj_auto_{}'.format(i+pad), 'rb')
        routes = pickle.load(routes_file)
        routes_file.close()
        map_timestamped_routes_int(routes, mapping)
        routes_file = open('traj_auto_{}_mapped_timeint'.format(i+pad), 'wb')
        pickle.dump(routes, routes_file)
        routes_file.close()


def __timestamp_dict__(routes):
    """ Builds a dict whose keys are the nodes and whole values are the set
    of timestamps at which the node was visited. """
    nodes_timestamped = collections.defaultdict(list)
    for route in routes:
        for (node_u, node_v) in route:
            nodes_timestamped[node_u[0]].append(node_u[1])
            nodes_timestamped[node_v[0]].append(node_v[1])
    return nodes_timestamped


def timestamp_dict_wrapper(start, end, pad):
    """ Builds a dict whose keys are the nodes and whole values are the set
    of timestamps at which the node was visited. """
    nodes_timestamped = collections.defaultdict(list)
    for i in range(start, end, pad):
        print(i)
        routes_file = open('traj_auto_{}_mapped_timeint'.format(i+pad), 'rb')
        routes = pickle.load(routes_file)
        routes_file.close()
        for route in routes:
            for (node_u, node_v) in route:
                nodes_timestamped[node_u[0]].append(node_u[1])
                nodes_timestamped[node_v[0]].append(node_v[1])
    return nodes_timestamped


def routes_nodes_to_set(routes):
    """ Puts all nodes in a set """
    nodes = set()
    for route in routes:
        for (node_u, node_v) in route:
            nodes.add(node_u)
            nodes.add(node_v)
    return nodes
    # len(nodes) == 17490


def merge_files():
    """ Merge the 16 routes files. Doesn't work, too much RAM. """
    # file1 = open('traj_auto_100000_mapped_timeint', 'rb')
    # file2 = open('traj_auto_200000_mapped_timeint', 'rb')
    # file3 = open('traj_auto_300000_mapped_timeint', 'rb')
    # file4 = open('traj_auto_400000_mapped_timeint', 'rb')
    # file5 = open('traj_auto_500000_mapped_timeint', 'rb')
    # file6 = open('traj_auto_600000_mapped_timeint', 'rb')
    # file7 = open('traj_auto_700000_mapped_timeint', 'rb')
    # file8 = open('traj_auto_800000_mapped_timeint', 'rb')
    # file9 = open('traj_auto_900000_mapped_timeint', 'rb')
    # file10 = open('traj_auto_1000000_mapped_timeint', 'rb')
    # file11 = open('traj_auto_1100000_mapped_timeint', 'rb')
    # file12 = open('traj_auto_1200000_mapped_timeint', 'rb')
    # file13 = open('traj_auto_1300000_mapped_timeint', 'rb')
    # file14 = open('traj_auto_1400000_mapped_timeint', 'rb')
    # file15 = open('traj_auto_1500000_mapped_timeint', 'rb')
    # file16 = open('traj_auto_1600000_mapped_timeint', 'rb')

    # a = pickle.load(file1)
    # b = pickle.load(file2)
    # c = pickle.load(file3)
    # d = pickle.load(file4)
    # e = pickle.load(file5)
    # f = pickle.load(file6)
    # g = pickle.load(file7)
    # h = pickle.load(file8)
    # ii = pickle.load(file9)
    # jj = pickle.load(file10)
    # k = pickle.load(file11)
    # ll = pickle.load(file12)
    # m = pickle.load(file13)
    # n = pickle.load(file14)
    # o = pickle.load(file15)
    # p = pickle.load(file16)

    # res = a+b+c+d+e+f+g+h+ii+jj+k+ll+m+n+o+p

    # file1.close()
    # file2.close()
    # file3.close()
    # file4.close()
    # file5.close()
    # file6.close()
    # file7.close()
    # file8.close()
    # file9.close()
    # file10.close()
    # file11.close()
    # file12.close()
    # file13.close()
    # file14.close()
    # file15.close()
    # file16.close()

    # del a
    # del b
    # del c
    # del d
    # del e
    # del f
    # del g
    # del h
    # del ii
    # del jj
    # del k
    # del ll
    # del m
    # del n
    # del o
    # del p

    # file17 = open('traj_timestamped_mapped_timeint', 'wb')
    # pickle.dump(res, file17)
    # file17.close()


def build_graph(net_file):
    """ Builds the unweighted graph """
    net = load_network(net_file)
    edge_list = parse_edges(net)
    # Instead of recomputing the mapping, work with the previous mapping to
    # keep the data consistent. See get_mapping()
    # mapping = get_mapping()
    mapping = dict()
    # Replaces all non integer nodes by integer nodes. The mapping is stored in
    # the MAPPING variable.
    # mapping = map_edges_int(EDGE_LIST)[1]
    edge_list, mapping = map_edges_int(edge_list, mapping)
    return edge_list, mapping


def build_routes(net_file, routes_file, mapping):
    """ Load non timestamped routes """
    net = load_network(net_file)
    # Using the old version because it is easier to avoid the bogus 'b32' edge
    routes = parse_routes_old(net, routes_file)
    # routes = parse_routes(net, routes_file)
    # mapping = get_mapping_reversed()
    # Replaces all non integer nodes according to MAPPING
    map_routes_int(routes, mapping)
    return routes


def build_timestamped_routes(net_file, routes_file, mapping):
    """ Builds timestamped routes. Non multithreaded for Bologna """
    net = load_network(net_file)
    routes = list()
    for (index, vehicle) in enumerate(sumolib.output.parse(routes_file,
                                                           "vehicle")):
        compute_timestamped_traj(net, vehicle.depart,
                                 vehicle.getChildList()[0].edges.split(),
                                 index, routes)
    map_timestamped_routes_int(routes, mapping)
    timestamp_dict = __timestamp_dict__(routes)
    return timestamp_dict


def build_timestamped_routes_multithreaded(routes_file, mapping):
    """ Builds timestamped routes. Multithreaded for Cologne

    ==================================================
    || This needs the NET variable to be set first. ||
    ==================================================

    """
    # use get_mapping_reversed() for Cologne
    start = 0
    end = get_length(routes_file)
    pad = 100000
    # Load timestamped routes (5h30 long):
    # Creates a bunch of files, in which there are lists of trajectories, which
    # is a list of pair of timestamped nodes (couple of node and timestamp).
    print("Starting")
    multithread_timestamped_traj(routes_file, start, end, pad)
    map_routes_int_timestamp(start, end, pad, mapping)
    timestamp_dict = timestamp_dict_wrapper(start, end, pad)
    # Does not work:
    # merge_files()
    return timestamp_dict


# if __name__ == "__main__":
    # Cologne export (5-6 hours):
    # NET = load_network(NET_FILE)
    # NODE_LIST = parse_nodes(NET)
    # EDGE_LIST, MAPPING = build_graph(NET_FILE)
    # check_edge_list_int(EDGE_LIST)
    # COORDS = build_coordinates(NET_FILE, get_mapping_reversed())
    # ROUTES = build_routes(NET_FILE, ROUTES_FILE, build_graph(NET_FILE)[1])
    # check_routes_int(ROUTES)
    # TIME = build_timestamped_routes_multithreaded(ROUTES_FILE,
    #                                               build_graph(NET_FILE)[1])
    # dump_net_to_file('cologne.graph', EDGE_LIST)
    # dump_routes_to_file('cologne.coords', COORDS)
    # dump_routes_to_file('cologne.routes', ROUTES)
    # dump_routes_to_file('cologne.time', TIME)

    # Very long, about 15 hours.
    # consistency(ROUTES, EDGE_LIST)

    # Bologne export:
    # n = load_network(BOLOGNE_NET)
    # e, m = build_graph(BOLOGNE_NET)
    # c = build_coordinates(BOLOGNE_NET, build_graph(BOLOGNE_NET)[1])
    # r = build_routes(BOLOGNE_NET, BOLOGNE_ROUTES,
    #                  build_graph(BOLOGNE_NET)[1])
    # t = build_timestamped_routes(BOLOGNE_NET, BOLOGNE_ROUTES,
    #                              build_graph(BOLOGNE_NET)[1])
    # dump_net_to_file('bologne.graph', e)
    # dump_routes_to_file('bologne.coords', c)
    # dump_routes_to_file('bologne.routes', r)
    # dump_routes_to_file('bologne.time', t)
